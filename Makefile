#   Copyright information
#
#       Copyright (C) 2004-2008 Thomas Sattler <tsattler@users.sourceforge.net>
#       Copyright (C) 2008-2009 Jari Aalto <jari.aalto@cante.net>
#
#   License
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program. If not, see <http://www.gnu.org/licenses/>.

DESTDIR		=
prefix		= /usr
exec_prefix	= $(prefix)
man_prefix	= $(prefix)/share

PACKAGE		= gt5
INSTALL		= /usr/bin/install
INSTALL_BIN	= $(INSTALL) -m 755
INSTALL_DATA	= $(INSTALL) -m 644
INSTALL_OBJS	= $(PACKAGE)

MANDIR1		= $(DESTDIR)$(man_prefix)/man/man1
BINDIR		= $(DESTDIR)$(exec_prefix)/bin
SBINDIR		= $(DESTDIR)$(exec_prefix)/sbin
ETCDIR		= $(DESTDIR)$/etc/$PACKAGE
SHAREDIR	= $(DESTDIR)$(prefix)/share/$(PACKAGE)
LIBDIR		= $(DESTDIR)$(prefix)/lib/$(PACKAGE)

INSTALL_OBJS_MAN1  = *.1

all: build

build:
	@echo gt5: nothing to build.

clean:
	-rm -f *[#~] *.\#* *.o *.exe core *.stackdump

distclean: clean

install-etc:
	$(INSTALL_BIN) -d $(ETCDIR)
	$(INSTALL_BIN)	  $(INSTALL_OBJS_ETC) $(ETCDIR)

install-bin:
	$(INSTALL_BIN) -d $(BINDIR)
	$(INSTALL_BIN)    $(INSTALL_OBJS) $(BINDIR)

install-man:
	$(INSTALL_BIN) -d $(MANDIR1)
	$(INSTALL_DATA) $(INSTALL_OBJS_MAN1) $(MANDIR1)

install: install-bin install-man

# End of file
